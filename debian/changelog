vrfy (990522-11) unstable; urgency=medium

  * New maintainer upload. (Closes: #920081)
  * Bumped DH level to 13
  * debian/control:
      - (Build-Depends): changed from 'debhelp' to 'debhelper-compat'.
      - (Maintainer): updated to new maintainer
      - (Rules-Requires-Root): added flag with value no.
      - (Standards-Version): Update to 4.5.0.
      - (Vcs-*): updated to salsa.debian.org
  * debian/compat:
      - removed legacy file.
  * debian/copyright:
      - Updated copyright of debian/*
  * debian/patches:
      - (00-Makefile.patch) modified to allow installation as
        default user and group, instead of forcing root:root.
  * debian/rules:
      - (DEB_LDFLAGS_MAINT_APPEND) removed legacy export value.
  * debian/tests/control:
      - Created to perform a trivial test.
  * debian/watch: added a fake site to explain about the current
      status of the original upstream homepage.

 -- Pedro Loami Barbosa dos Santos <pedro@loami.eng.br>  Sat, 09 May 2020 15:09:17 -0300

vrfy (990522-10) unstable; urgency=medium

  * debian/control
    - (Standards-Version): Update to 3.9.8.
    - (Vcs-*): Update to anonscm.debian.org.
  * debian/copyright
    - Correct license short name from BSD to BDS-2-clause.
  * General note:
    - Manual page is installed (Closes: 792604).
      Thanks to Florian Schlichting <fsfs@debian.org>.

 -- Jari Aalto <jari.aalto@cante.net>  Thu, 20 Oct 2016 17:11:07 +0300

vrfy (990522-9) unstable; urgency=low

  * debian/control
    - (Homepage): Delete. Dead upstream.
    - (Standards-Version): Update to 3.9.5.
    - (Vcs-*): Update to anonscm.debian.org.
  * debian/copyright
    - Update years.
    - (Upstream-Contact, Source): Empty.
  * debian/patches:
    - (21): Improve headers.

 -- Jari Aalto <jari.aalto@cante.net>  Fri, 21 Feb 2014 16:53:30 +0200

vrfy (990522-8) unstable; urgency=low

  * debian/control
    - (Build-Depends): Rm dpkg-dev; not needed with debhelper 9.
    - (Standards-Version): Update to 3.9.3.1.
  * debian/copyright
    - Update to format 1.0.
  * debian/rules
    - Enable all hardening flags.
    - Use DEB_*_MAINT_* variables.

 -- Jari Aalto <jari.aalto@cante.net>  Sat, 24 Mar 2012 01:28:39 -0400

vrfy (990522-7) unstable; urgency=low

  * debian/compat
    - Update to 9
  * debian/control
    - (Build-Depends): update to debhelper 9, dpkg-dev 1.16.1.
    - (Standards-Version): update to 3.9.2.
  * debian/rules
    - Delete unused targets.
    - Use hardened CFLAGS.
      http://wiki.debian.org/ReleaseGoals/SecurityHardeningBuildFlags

  [tony mancill]
  * Add Vcs-* fields to d/control.

 -- Jari Aalto <jari.aalto@cante.net>  Tue, 14 Feb 2012 16:07:20 -0500

vrfy (990522-6) unstable; urgency=low

  * New mainainer (Closes: #486859).
  * debian/control
    - (Depends): add ${misc:Depends}.
    - (Standards-Version): update to 3.8.4.
  * debian/copyright
    - Correct to layout format http://dep.debian.net/deps/dep5
  * debian/rules
    - update to dh(1).
  * debian/patches/series
    - Completely rearrange patches and split old massive patch
      into pieces.
    - (Number 20): New. Fix implicit declaration of function _getshort.

 -- Jari Aalto <jari.aalto@cante.net>  Mon, 15 Mar 2010 03:18:32 +0200

vrfy (990522-5) unstable; urgency=low

  * QA upload.
  * Converted to "3.0 (quilt)" format.
  * debian/control:
    - Bumped Standards-Version to 3.8.3.
    - Changed Maintainer to Debian QA Group <packages@qa.debian.org>.
    - Added versioned dependency on debhelper (>= 7).
    - Added Homepage.
    - Added dependency on quilt.
  * debian/rules:
    - Cleaned up unecessary comments.
    - Removed "-" before the make clean.
    - Changed DESTDIR from debian/tmp to debian/vrfy.
  * debian/copyright:
    - Updated copyright format.
  * debian/source/format:
    - New file set to "3.0 (quilt)".
  * debian/compat:
    - New file set to "7".
  * debian/watch:
    - New file stating that upstream URL is unavailable.
  * debian/patches:
    - 00_fix_old_fixes.diff: includes all changes made by original maintainer
      outside the debian/ directory.
    - 01_dash_in_mail.diff: adds possibility to verify addresses starting
      with "-" (e.g. -foo@bar.org). (Closes: #405761)
    - 02_manpage_type.diff: corrects a typo error in the manpage.
      (Closes: #337201)

 -- Ignace Mouzannar <mouzannar@gmail.com>  Mon, 16 Nov 2009 23:01:37 +0100

vrfy (990522-4) unstable; urgency=low

  * Lowered compiler optimization level to fix segfault (Closes: #262573).

 -- Mark Suter <suter@zwitterion.humbug.org.au>  Mon,  2 Aug 2004 19:40:44 +1000

vrfy (990522-3) unstable; urgency=low

  * New maintainer (Closes: #249717)

 -- Mark Suter <suter@zwitterion.humbug.org.au>  Wed, 26 May 2004 21:07:35 +1000

vrfy (990522-2) unstable; urgency=low

  * Replaced sys_errlist with strerror (Nick Rusnov, closes: #106920).

 -- Herbert Xu <herbert@debian.org>  Mon, 30 Jul 2001 20:44:16 +1000

vrfy (990522-1) unstable; urgency=low

  * New upstream release.
  * Description reformatted by Josip Rodin (fixes #42095).

 -- Herbert Xu <herbert@debian.org>  Sat, 25 Sep 1999 16:11:29 +1000

vrfy (980626-1) unstable; urgency=low

  * Initial Release.

 -- Herbert Xu <herbert@debian.org>  Wed, 22 Jul 1998 18:04:17 +1000
